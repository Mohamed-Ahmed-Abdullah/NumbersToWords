﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cucumber.Core.Converters;

namespace Cucumber.Services
{
  public interface ICurrencyService
  {
    Currency Currency { get; }
  }
}
