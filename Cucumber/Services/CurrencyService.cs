﻿using System.Collections.Generic;
using System.Linq;
using Cucumber.Core.Converters;
using Cucumber.Core.Converters.CurrencySettings;
using Microsoft.Extensions.Configuration;

namespace Cucumber.Services
{
  public class CurrencyService : ICurrencyService
  {
    private IConfiguration _Configuration;
    public Currency Currency { get; }

    public CurrencyService(IConfiguration configuration)
    {
      _Configuration = configuration;

      Currency = new Currency(new Settings
      {
        Mode = Mode.Casual,
        One = FillSection("one"),
        Two = FillSection("two"),
        Tens = FillSection("tens"),
        TensPower = FillSection("tensPower"),
        Others = FillSection("others"),
      });
    }

    private Dictionary<string, string> FillSection(string sectionName)
    {
      var sectionOne = _Configuration.GetSection(sectionName);
      return sectionOne.GetChildren().ToDictionary(d => d.Key, v => v.Value);
    }
  }
}
