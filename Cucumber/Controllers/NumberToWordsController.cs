using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cucumber.Services;
using Microsoft.AspNetCore.Mvc;

namespace Cucumber.Controllers
{
  [Route("api/[controller]")]
  public class NumberToWordsController : Controller
  {
    private ICurrencyService _currencyService;

    public NumberToWordsController(ICurrencyService currencyService) => _currencyService = currencyService;

    //TODO: We should accept the Casual/Formal Settings from the UI
    //TODO: if the user enter 12$ or 12USD it should be allowed, we should handle the currency better 
    [HttpGet("[action]")]
    public JsonResult GetWords(string username, string number)
    {
      //TODO: This is unsafe Parsing please change it to more reliable one
      return Json(_currencyService.Currency.GetString(decimal.Parse(number)));
    }
  }
}
