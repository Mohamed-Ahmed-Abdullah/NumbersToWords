import { Component, Inject} from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { HttpErrorResponse } from "@angular/common/http";
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent
{

    constructor(private _http: HttpClient, @Inject('BASE_URL') base: string) {
        this.baseUrl = base;
    }

    numberAsWords: string ='Written Number will be shown here';
    baseUrl: string;
    userName: string;
    number: string;

    submit() {
        if (!this.userName || !this.number) {
            //TODO: check if the number is digits
            alert("Please Make sure to enter your name and money amount please");
            return;
        }

        this._http.get<string>(this.baseUrl + 'api/NumberToWords/GetWords',
            { params: new HttpParams({ fromObject: { username: this.userName, number: this.number } }) }
        ).subscribe(result => {
            this.numberAsWords = result;
        }, error => console.error(error));
    }
}
