﻿namespace Cucumber.Core.Helpers
{
  public static class Extensions
  {
    public static string AddSpace(this string text)
    {
      return $"{text} ";
    }
    public static string Add(this string text, string and)
    {
      return $"{text} {and} ";
    }
    public static bool HasFraction(this decimal number)
    {
      return number % 1 != 0;
    }
  }

}
