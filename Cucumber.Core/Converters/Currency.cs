﻿using System;
using System.Collections.Generic;
using Cucumber.Core.Converters.CurrencySettings;
using Cucumber.Core.Helpers;

namespace Cucumber.Core.Converters
{
  public class Currency
  {
    //TODO: Correct the placement of "and" in the statement based on the requirements
    //TODO: Add - when it's Hundred ex. Forty-Five as per the requirements
    //TODO: Pay attention to the plural or singular ex. 12 dollars 1 dollar 
    public ISettings Settings { get; set; }

    public Currency(ISettings settings) => Settings = settings;

    public string GetString(decimal amount)
    {
      if (Settings == null) { UseDefaultSettings(); }

      var text = "";

      if (amount == 0)
        return Settings.One["0"].Add(Settings.CurrencyName);

      if (amount < 0)
      {
        text += Settings.Others["Minus"].AddSpace();
        amount = Math.Abs(amount);
      }

      var level = PowerLevel(amount);
      if (level > 24)
        throw new Exception("This Number is Too big");

      if (amount.HasFraction())
      {
        var parts = amount.ToString().Split('.');
        text += $"{Reader(decimal.Parse(parts[0]), false)}{Settings.CurrencyName}{Settings.And}{Reader(decimal.Parse(parts[1]), true)}{Settings.Fraction}";
        return text;
      }

      text += Reader(amount, false).Add(Settings.CurrencyName);

      return text;
    }

    private string Reader(decimal amount, bool isFraction)
    {
      var text = "";
      var digits = amount.ToString().Length;
      var chunk = digits;

      if (Settings.Mode == Mode.Casual && amount > 100 && amount < 10000 && digits == 4 && !isFraction)
      {
        //12,00 the statement is "Twelve Hundred" and 12 is the firstPart
        var firstPart = decimal.Truncate(amount / 100);
        var fraction = amount - (firstPart * 100m);
        return $"{ReadableChunk(firstPart)}{Settings.Hundred}{((fraction != 0) ? Settings.And + ReadableChunk(fraction) + Settings.CurrencyName : "")}";
      }

      do
      {
        //First Step
        var stepSize = chunk % 3;
        if (stepSize == 0)
        {
          stepSize = 3;
        }

        var chunkAmount = decimal.Truncate(amount / (long)Math.Pow(10, chunk - stepSize));
        amount -= chunkAmount * (long)Math.Pow(10, chunk - stepSize);
        text += ReadableChunk(chunkAmount) + ((chunk - stepSize == 0) ? "" : Settings.TensPower[(chunk - stepSize).ToString()].AddSpace());
        chunk = chunk - stepSize;

      } while (chunk > 0 && amount > 0);

      return text;
    }

    private string ReadableChunk(decimal amount)
    {
      var text = "";
      var isFirstTime = true;
      do
      {
        var level = PowerLevel(amount);
        if (level == 0)
        {
          text += LevelZero(ref amount);
          isFirstTime = false;
        }
        else if (level == 1 && amount < 20)
        {
          text += LevelTeen(ref amount);
          isFirstTime = false;
        }
        else if (level == 1 && amount >= 20)
        {
          text += LevelOne(ref amount);
          isFirstTime = false;
        }
        else if (level == 2)
        {
          text += LevelTwo(ref amount);
          isFirstTime = false;
        }
        else
        {
          throw new Exception("This is not Readable Chunk, Please consider breaking down the number.");
        }
      } while (amount > 0);

      return text;
    }

    private string LevelTwo(ref decimal amount)
    {
      var step = decimal.Truncate(amount / 100);
      amount -= step * 100;
      return Settings.One[step.ToString()].AddSpace() +
             Settings.Others["Hundred"].AddSpace();
    }

    private string LevelOne(ref decimal amount)
    {
      var step = decimal.Truncate(amount / 10);
      amount -= step * 10;
      return Settings.Tens[step.ToString()].AddSpace();
    }

    private string LevelTeen(ref decimal amount)
    {
      var step = amount - 10;
      amount = 0;
      return Settings.Two[step.ToString()].AddSpace();
    }

    private string LevelZero(ref decimal amount)
    {
      var text = Settings.One[decimal.Truncate(amount).ToString()].AddSpace();
      amount = 0;
      return text;
    }


    /// <summary>
    /// 1 = 0, 9 = 0, 10 = 1, 99 = 1, 100 = 2,...
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    private int PowerLevel(decimal number)
    {
      //Get the log then truncate by the casting
      return (int)Math.Log10((double)number);
    }

    /// <summary>
    /// 1 = 0, 200 = 0, 2,000=1, 2,000,000 = 2, ...
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    private int Comma(decimal number)
    {
      //No negative values and truncate by the casting
      return Math.Max(0, ((int)number / 1000) - 1);
    }

    private string GetWords(string number)
    {
      return "";
    }

    private void UseDefaultSettings()
    {
      Settings = new Settings
      {
        //TODO: Fill the default...
        Mode = Mode.Formal,
        One = new Dictionary<string, string> { { "", "" }, }
      };
    }
  }
}
