﻿namespace Cucumber.Core.Converters.CurrencySettings
{
  public enum Mode
  {
    /// <summary>
    /// 1200 is one thousand and two hundred
    /// </summary>
    Formal,

    /// <summary>
    /// 1200 twelve hundred
    /// </summary>
    Casual
  }
}
