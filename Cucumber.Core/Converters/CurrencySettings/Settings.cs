﻿using System.Collections.Generic;
using Cucumber.Core.Helpers;

namespace Cucumber.Core.Converters.CurrencySettings
{
  public class Settings : ISettings
  {
    public Mode Mode { get; set; }
    public Dictionary<string, string> One { get; set; }
    public Dictionary<string, string> Two { get; set; }
    public Dictionary<string, string> Tens { get; set; }
    public Dictionary<string, string> TensPower { get; set; }
    public Dictionary<string, string> Others { get; set; }

    public string And => Others["And"].AddSpace();
    public string Fraction => Others["CurrencyFraction"].AddSpace();
    public string CurrencyName => Others["CurrencyName"].AddSpace();
    public string Hundred => Others["Hundred"].AddSpace();
  }
}
