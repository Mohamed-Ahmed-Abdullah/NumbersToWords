﻿using System.Collections.Generic;

namespace Cucumber.Core.Converters.CurrencySettings
{
  public interface ISettings
  {
    Mode Mode { get; set; }
    Dictionary<string, string> One { get; set; }
    Dictionary<string, string> Two { get; set; }
    Dictionary<string, string> Tens { get; set; }
    Dictionary<string, string> TensPower { get; set; }
    Dictionary<string, string> Others { get; set; }
    string And { get; }
    string Fraction { get; }
    string CurrencyName { get; }
    string Hundred { get; }
  }
}
