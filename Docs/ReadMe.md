# Cucumber Number To Words

This is a webapp that allows you to enter an amount of money and it will show you the words that corespond to that amount for example 12 will be Tweleve Dollars and so on.
![alt text](https://raw.githubusercontent.com/Mohamed-Ahmed-Abdullah/NumbersToWords/master/Docs/images/home.png)

# Prerequisites
#### Build Prerequisites
- Visual Studio 2017
- .Net Framework and .NetCore 
- node.js
- IIS >= 10
#### Runtime Prerequisites
- IIS >= 10
- .Net Core 2.2

# How to build and run 
#### How to build
- Change the $solutionPath to your local solution path  
- run ./build.ps1
```sh
PS> ./build.ps1
```
#### How to deploy 
- set your desigred $siteName 
- set the $sitePath
- set your $solutionFolder

Then you can run the next command
```sh
PS> ./deploy.ps1
```

# Technical	Description
This solution is divided into couple of sections for couple of reasons:

1- The furure maintanability will be easy.

2- To make the process of packaging and deploying easy

3- To allow the solution to be deployed as a standalone site or as a Sitecore package

## The Projects Structure
- Cucumber.Core is the main project which contains the curency conversion functionality
- Cucumber (Web Project) the web project is referencing the Cucumber.Core and adding the UI Layer and the API Layer
- Cucumber.Content is referencing both of the preior projects to act as a single package to make the instalation easy. for example if someone wants to install this solution as a nuget package the Cucumber.Content will wrap the whole solution in one nuget package with it's dependancies, so it will be one install.

## The Test Projects
- Cucumber.Core.UnitTest is only testing the main functionality of the core project 
- Cucumber.IntegrationTests not entirely end to end testing as the UI is not part of the integration tests, althought it's important but it makes the itegration tests skwishy and error prone as there is a rapid changes in the UI. our intention is testing the APIs to give us more stability.


# Upcoming Features

  - Security 
  - CI Integration 
  - Gobalization & Localization 
  - Allowing the user to select the Mode(Formal/Casual) From the UI
  - Performance Test
  - Deploy as a standalone site or as a Sitecore package

# Upcoming Bug Fixes 
- [Correct the placement of "and" in the statement based on the requirements][currency]
- [Add dash or - when it's hundred ex. Forty-Five. follow per the requirements][currency]
- [Pay attention to the plural or singular ex. 12 dollars 1 dollar][currency]
- [If the user enter 12$ or 12USD it should be allowed, we should handle the currency better][api]
- [We should Read this from xml or json settings file, to allow extensibility in the future][start]
- [Create a proper UI testing for the Interface][test]

[currency]: <https://github.com/Mohamed-Ahmed-Abdullah/NumbersToWords/blob/master/Cucumber.Core/Converters/Currency.cs>
[api]: <https://github.com/Mohamed-Ahmed-Abdullah/NumbersToWords/blob/master/Cucumber/Controllers/NumberToWordsController.cs>
[start]: <https://github.com/Mohamed-Ahmed-Abdullah/NumbersToWords/blob/master/Cucumber/Startup.cs>
[test]: <https://github.com/Mohamed-Ahmed-Abdullah/NumbersToWords/blob/master/Tests/Cucumber.IntegrationTests/Smoke.cs>