$siteName = "Cucumber" 
$sitePath = "C:\inetpub\wwwroot\$siteName"
$hostFile = "C:\Windows\System32\Drivers\etc\hosts"
$solutionFolder = "C:\Users\maam\Documents\Visual Studio 2017\projects\Cucumber"

# Clean First
Stop-WebAppPool $siteNamename -ErrorAction Stop
Remove-WebAppPool $siteName -ErrorAction Continue
Stop-Website $siteName
Remove-Website $siteNam
Remove-Item -Path $sitePath

# Create the Site
$appPool = New-WebAppPool -Name $siteName
$siteId = (dir iis:\sites | foreach {$_.id} | sort -Descending | select -first 1 )+ 1
New-Item -ItemType directory -Path $sitePath
New-Website -name $siteName -ApplicationPool $siteName -PhysicalPath $sitePath -HostHeader $siteName -id $siteId

# Install the Packages
#TODO: The WDP package are not created at the moment make sure the package is created then ship it to the site path  
New-Item -ItemType directory -Path "$sitePath\ClientApp"
Copy-Item -Path "$solutionFolder\Cucumber\ClientApp\dist" -Destination "$sitePath\ClientApp" -Recurse
Copy-Item -Path "$solutionFolder\Cucumber\wwwroot" -Destination "$sitePath\wwwroot" -Recurse

New-Item -ItemType directory -Path "$sitePath\Config"
Copy-Item -Path "$solutionFolder\Cucumber.Content\bin\Release\netcoreapp2.1\Config\En.json" -Destination "$sitePath\Config\En.json"
Copy-Item -Path "$solutionFolder\Cucumber.Content\bin\Release\netcoreapp2.1\Cucumber.Core.dll" -Destination "$sitePath\Cucumber.Core.dll"
Copy-Item -Path "$solutionFolder\Cucumber.Content\bin\Release\netcoreapp2.1\Cucumber.dll" -Destination "$sitePath\Cucumber.dll"

Copy-Item -Path "$solutionFolder\Cucumber\Cucumber.runtimeconfig.json" -Destination "$sitePath\Cucumber.runtimeconfig.json" -Recurse
Copy-Item -Path "$solutionFolder\Cucumber\web.config" -Destination "$sitePath\web.config" -Recurse

#add the sitename to the host file 
$newLine = [Environment]::NewLine
$data = Get-Content -Path $hostFile             
$data += $newLine + "127.0.0.1`t$siteName"
$encoding = New-Object System.Text.UTF8Encoding($False)
[System.IO.File]::WriteAllLines($hostFile, $data, $encoding)

# ping the site
Start-Process "http://$siteName"