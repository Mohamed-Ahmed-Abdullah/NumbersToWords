using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Should;
using Xunit;


namespace Cucumber.IntegrationTests
{
  public class Smoke
  {
    //TODO: Testing the Api is more stable, but smoke testing the UI is important 
    [Fact]
    public async Task Zero()
    {

      var baseUrl = ConfigurationManager.AppSettings["baseUrl"];
      var url = $"{baseUrl}/api/NumberToWords/GetWords?username=sad&number=12";
      var client = new HttpClient();
      var response = await client.GetAsync(url);

      response.StatusCode.ShouldEqual(HttpStatusCode.OK);
      var words = await response.Content.ReadAsStringAsync();
      words.ShouldEqual("\"Twelve  Dollar  \"");
    }
  }
}
