using Cucumber.Core.Converters;
using Cucumber.Core.Converters.CurrencySettings;
using Should;
using System.Collections.Generic;
using Xunit;

namespace Cucumber.Core.UnitTest
{
  public class CurrencyTest
  {
    [Fact]
    public void Zero()
    {
      Get(0).Trim().ShouldEqual("Zero Dollar");
    }
    [Fact]
    public void MinusOneDollar() { Get(-1).Trim().ShouldEqual("Minus One  Dollar"); }
    [Fact]
    public void OneDollar() { Get(1).Trim().ShouldEqual("One  Dollar"); }
    [Fact]
    public void NineDollar() { Get(9).Trim().ShouldEqual("Nine  Dollar"); }
    [Fact]
    public void TenDollar() { Get(10).Trim().ShouldEqual("Ten  Dollar"); }

    [Fact]
    public void ElevenDollar()
    {
      Get(11).Trim().ShouldEqual("Eleven  Dollar");
    }

    [Fact]
    public void TwentyOneDollar()
    {
      Get(21).Trim().ShouldEqual("Twenty One  Dollar");
    }

    [Fact]
    public void OneHundredTwentyOneDollar()
    {
      Get(121).Trim().ShouldEqual("One Hundred Twenty One  Dollar");
    }

    [Fact]
    public void ElevenThousandOneDollar()
    {
      Get(11001).Trim().ShouldEqual("Eleven Thousand One  Dollar");
    }

    [Fact]
    public void TwoHundredTwentyTwoThousandThreeHundredTwoDollar()
    {
      Get(222302).Trim().ShouldEqual("Two Hundred Twenty Two Thousand Three Hundred Two  Dollar");
    }

    [Fact]
    public void ElevenDollarAndThirtyFourCent()
    {
      Get(11.34m).Trim().ShouldEqual("Eleven Dollar And Thirty Four Cent");
    }

    [Fact]
    public void ElevenHundredAndOneDollarDollarAndThirtyFourCent()
    {
      Get(1101.34m).Trim().ShouldEqual("One Thousand One Hundred One Dollar And Thirty Four Cent");
    }

    [Fact]
    public void TwelveHundredDollar()
    {
      Get(1200,Mode.Casual).Trim().ShouldEqual("Twelve Hundred  Dollar");
    }

    [Fact]
    public void OneThousandTwoHundredDollar()
    {
      Get(1200, Mode.Formal).Trim().ShouldEqual("One Thousand Two Hundred  Dollar");
    }

    public string Get(decimal amount, Mode mode = Mode.Formal)
    {
      var currency = new Currency(GetSettings(mode));
      return currency.GetString(amount);
    }

    private ISettings GetSettings(Mode mode)
    {
      return new Settings
      {
        Mode = mode,
        One = new Dictionary<string, string>
        {
          {"0", "Zero"},
          {"1", "One"},
          {"2", "Two"},
          {"3", "Three"},
          {"4", "Four"},
          {"5", "Five"},
          {"6", "Six"},
          {"7", "Seven"},
          {"8", "Eight"},
          {"9", "Nine"}
        },
        Two = new Dictionary<string, string>
        {
          {"0", "Ten"},
          {"1", "Eleven"},
          {"2", "Twelve"},
          {"3", "Thirteen"},
          {"4", "Fourteen"},
          {"5", "Fifteen"},
          {"6", "Sixteen"},
          {"7", "Seventeen"},
          {"8", "Eighteen"},
          {"9", "Nineteen"}
        },
        Tens = new Dictionary<string, string>
        {
          {"0", ""},
          {"1", "Ten"},
          {"2", "Twenty"},
          {"3", "Thirty"},
          {"4", "Forty"},
          {"5", "Fifty"},
          {"6", "Sixty"},
          {"7", "Seventy"},
          {"8", "Eighty"},
          {"9", "Ninety"}
        },
        TensPower = new Dictionary<string, string>
        {
          {"3", "Thousand"},
          {"6", "Million"},
          {"9", "Billion"},
          {"12", "Trillion"},
          {"15", "Quadrillion"},
          {"18", "Quintillion"},
          {"21", "Sextillion"},
          {"24", "Septillion"}
        },
        Others = new Dictionary<string, string>
        {
          {"Hundred", "Hundred"},
          {"And", "And"},
          {"Minus", "Minus"},
          {"CurrencyName", "Dollar"},
          {"CurrencyFraction", "Cent"}
        }
      };
    }
  }
}

